<?php

return [
    'FLG_OFF' => '0',
    'FLG_ON'  => '1',
    // 修正
    'FLG_PROCESSING'  => '-1',

    'PAGINATE_LIMIT'    => '15',
    'REVIEW_COMMENT_LIMIT'  => '10',
    'TEMP_USER_ID' => '1234567890',
    'ACTIVE_RATE_STRING'   => '★',
    'INACTIVE_RATE_STRING' => '☆',
    'RATE_LIMIT'           => '5',
    'EXPORT_REVIEWS_FILE_NAME' => 'reviews',
    'CSV_ITEMS' => [
        'REVIEW' => [
            'review_id', 'gmb_name', 'gmb_reviewid',
            'gmb_profile_photo_url', 'gmb_display_name',
            'gmb_is_anonymous', 'gmb_star_rating', 'gmb_comment',
            'gmb_create_date'
        ]
    ],
    'CSV_HEADERS' => [
        'REVIEW' => [
            'ID','投稿者名','投稿者ID','投稿者プロフィールURL',
            '投稿者表示名', '匿名投稿','評点','コメント','投稿日時'
        ]
    ],
    // 修正
    'IS_DELETED' => [
        'OFF' => 0,
        'ON' => 1,
        'PROCESSING' => -1
    ],  
    'SYNC_TYPE' => [
        'CREATE' => 'CREATE',
        'PATCH' => 'PATCH',
        'DELETE' => 'DELETE'
    ],   
    'SYNC_STATUS' => [
        'DRAFT' => 'DRAFT',
        'QUEUED' => 'QUEUED',
        'FAILED' => 'FAILED',
        'CANCEL' => 'CANCEL',
        'SYNCED' => 'SYNCED'
    ],   
    'ACTION_TYPE' => [
        'ACTION_TYPE_UNSPECIFIED' => 'ACTION_TYPE_UNSPECIFIED',
        'LEARN_MORE' => 'LEARN_MORE'
    ],      
    'TOPIC_TYPE' => [
        'LOCAL_POST_TOPIC_TYPE_UNSPECIFIED' => 'LOCAL_POST_TOPIC_TYPE_UNSPECIFIED',
        'STANDARD' => 'STANDARD',
        'EVENT' => 'EVENT',
        'OFFER' => 'OFFER',
        'ALERT' => 'ALERT',
    ],
    
    'METRIC' => [
        'QUERIES_DIRECT' => 'QUERIES_DIRECT',
        'QUERIES_INDIRECT' => 'QUERIES_INDIRECT',
        'QUERIES_CHAIN' => 'QUERIES_CHAIN',
        'VIEWS_MAPS' => 'VIEWS_MAPS',
        'VIEWS_SEARCH' => 'VIEWS_SEARCH',
        'ACTIONS_WEBSITE' => 'ACTIONS_WEBSITE',
        'ACTIONS_PHONE' => 'ACTIONS_PHONE',
        'ACTIONS_DRIVING_DIRECTIONS' => 'ACTIONS_DRIVING_DIRECTIONS',
        'PHOTOS_VIEWS_MERCHANT' => 'PHOTOS_VIEWS_MERCHANT',
        'PHOTOS_VIEWS_CUSTOMERS' => 'PHOTOS_VIEWS_CUSTOMERS',
        'PHOTOS_COUNT_MERCHANT' => 'PHOTOS_COUNT_MERCHANT',
        'PHOTOS_COUNT_CUSTOMERS' => 'PHOTOS_COUNT_CUSTOMERS',
        'LOCAL_POST_VIEWS_SEARCH' => 'LOCAL_POST_VIEWS_SEARCH',
        'LOCAL_POST_ACTIONS_CALL_TO_ACTION' => 'LOCAL_POST_ACTIONS_CALL_TO_ACTION'
    ],



];
