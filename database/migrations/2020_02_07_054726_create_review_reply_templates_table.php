<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewReplyTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_reply_templates', function (Blueprint $table) {
            $table->unsignedInteger('review_reply_template_id')->autoIncrement();
            $table->string('template_name',100)->nullable();
            $table->string('template',1000)->nullable();
            $table->tinyInteger('is_autoreply_template')->nullable();
            $table->tinyInteger('target_star_rating')->nullable();
            $table->tinyInteger('is_deleted')->nullable();
            $table->char('create_user_id', 10)->nullable();
            $table->dateTime('create_time')->nullable();
            $table->char('update_user_id', 10)->nullable();
            $table->dateTime('update_time')->nullable();
            //$table->primary('review_reply_template_id');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE review_reply_templates MODIFY review_reply_template_id int(11) unsigned NOT NULL AUTO_INCREMENT');
        DB::connection()->getPdo()->exec('ALTER TABLE review_reply_templates MODIFY is_autoreply_template tinyint(1) unsigned NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE review_reply_templates MODIFY target_star_rating tinyint(1) unsigned NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE review_reply_templates MODIFY is_deleted tinyint(1) unsigned NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_reply_templates');
    }
}
