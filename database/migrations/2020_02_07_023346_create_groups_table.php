<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->unsignedInteger('group_id');
            $table->unsignedInteger('organization_id');
            $table->primary('group_id');
            $table->index('organization_id','fk_groups_organizations1_idx');
            $table->foreign('organization_id','fk_groups_organizations1')
                ->references('organization_id')
                ->on('organizations');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE groups MODIFY group_id int(11) unsigned NOT NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE groups MODIFY organization_id int(11) unsigned NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
