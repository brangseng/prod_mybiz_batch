<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->unsignedInteger('location_id');
            $table->primary('location_id');
            $table->foreign('location_id','fk_addresses_locations1')
                ->references('location_id')
                ->on('locations');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE addresses MODIFY location_id int(11) unsigned NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
