<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->char('user_id',10);
            $table->unsignedInteger('groups_group_id');
            $table->primary('user_id');
            $table->index('groups_group_id','fk_users_groups1_idx');
            $table->foreign('groups_group_id','fk_users_groups1')
                ->references('group_id')
                ->on('groups');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE users MODIFY groups_group_id int(11) unsigned NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
