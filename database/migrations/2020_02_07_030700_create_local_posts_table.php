<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_posts', function (Blueprint $table) {
            $table->unsignedInteger('localposts_id');
            $table->string('gmb_name',100)->nullable();
            $table->string('gmb_language_code',100)->nullable();
            $table->string('gmb_summary',1000)->nullable();
            $table->enum('gmb_action_type',
                ['ACTION_TYPE_UNSPECIFIED', 'BOOK', 'ORDER', 'SHOP',
                    'LEARN_MORE', 'SIGN_UP', 'CALL'])->nullable();
            $table->string('gmb_action_type_url',100)->nullable();
            $table->dateTime('gmb_create_time')->nullable();
            $table->dateTime('gmb_update_time')->nullable();
            $table->string('gmb_event_title',100)->nullable();
            $table->dateTime('gmb_event_start_time')->nullable();
            $table->dateTime('gmb_event_end_time')->nullable();
            $table->enum('gmb_local_post_state',
                ['LOCAL_POST_STATE_UNSPECIFIED', 'REJECTED', 'LIVE', 'PROCESSING'])->nullable();
            $table->string('gmb_search_url',100)->nullable();
            $table->string('gmb_topic_type',45)->nullable();
            $table->string('gmb_offer_coupon_code',100)->nullable();
            $table->string('gmb_offer_redeem_online_url',100)->nullable();
            $table->string('gmb_offer_terms_condition',1000)->nullable();
            $table->tinyInteger('is_deleted')->nullable();
            $table->enum('sync_status',
                ['DRAFT', 'READY', 'QUEUED', 'SYNCED'])->nullable();
            $table->tinyInteger('is_scheduled')->nullable();
            $table->dateTime('scheduled_post_time')->nullable();
            $table->char('create_user_id', 10)->nullable();
            $table->dateTime('create_time')->nullable();
            $table->char('update_user_id', 10)->nullable();
            $table->dateTime('update_time')->nullable();
            $table->unsignedInteger('mediaitem_id')->nullable();
            $table->primary('localposts_id');
            $table->index('mediaitem_id','fk_localposts_mediaitem1_idx');
            $table->foreign('mediaitem_id','fk_localposts_mediaitem1')
                ->references('mediaitem_id')
                ->on('media_items');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE local_posts MODIFY localposts_id int(11) unsigned NOT NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE local_posts MODIFY is_deleted tinyint(1) unsigned NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE local_posts MODIFY is_scheduled tinyint(1) unsigned NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE local_posts MODIFY mediaitem_id int(11) unsigned NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_posts');
    }
}
