function deleteTemplate(templateId){
   'use strict';
   if(window.confirm('削除しますか？')){
     window.axios({
        url: '/template/delete',
        method: 'POST',
        data:{
         'review_reply_template_id':templateId
        }
     }).then(response =>  {
        location.reload();
     }).catch(error => {
     });
   }
}