@extends('layouts.app')
@section('title', '返信テンプレート変更')
@section('content')
<div class="col-md-8 order-md-1">
 <h4 class="mb-4">返信テンプレート変更</h4>
 <form action="{{url('/template/update')}}" method="post">
    @csrf
  <input type="hidden" name="review_reply_template_id" value="{{$template->review_reply_template_id}}">
  <div class="mb-4">
     <label for="" class="d-block font-weight-bold">テンプレート名</label>
     {{Form::text('template_name', old('template_name',$template->template_name), ['class' => ['form-control',($errors->has('template_name')) ? 'is-invalid':'']])}}
     @if ($errors->has('template_name'))
      <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('template_name') }}</strong>
      </span>
     @endif
  </div>
  <div class="mb-4">
   <label for="" class="d-block font-weight-bold">返信タイプ</label>
   <label class="form-check-label radio-inline mr-4" for="is_autoreply_template1">
     {{Form::radio('is_autoreply_template', config('const.FLG_ON'), (old('is_autoreply_template',$template->is_autoreply_template) == config('const.FLG_ON')), ['class'=>''])}}自動返信
   </label>
   <label class="form-check-label radio-inline" for="is_autoreply_template2">
    {{Form::radio('is_autoreply_template', config('const.FLG_OFF'), (old('is_autoreply_template',$template->is_autoreply_template) == config('const.FLG_OFF')), ['class'=>''])}}手動返信
   </label>
  </div>
  <div class="mb-4">
     <label for="" class="d-block font-weight-bold">自動返信クチコミ評点</label>
     {{Form::select('target_star_rating', config('formConst.rate'), old('target_star_rating',$template->target_star_rating), ['class' => ['mb-3 form-control',($errors->has('target_star_rating')) ? 'is-invalid':''],'placeholder' => '選択してください'])}}
     @if ($errors->has('target_star_rating'))
      <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('target_star_rating') }}</strong>
      </span>
     @endif
  </div>

  <div class="mb-4">
     <label for="template" class="font-weight-bold">返信内容</label>
     {{Form::textarea('template', old('template',$template->template),
     ['rows' => 8,'class'=>['form-control',($errors->has('template')) ? 'is-invalid':'']])}}
     @if ($errors->has('template'))
         <span class="invalid-feedback" role="alert">
             <strong>{{ $errors->first('template') }}</strong>
         </span>
     @endif
  </div>

   <div class="row p-3">
    <div class="mx-auto w-25">
        <button class="btn btn-primary btn-block" type="submit">変更</button>
    </div>
   </div>

 </form>
</div>
@endsection