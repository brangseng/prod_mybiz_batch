<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

// 修正
/*
Route::resource('v1.0/location', 'Api\v1_0\LocationController');
Route::resource('v1.0/localpost', 'Api\v1_0\LocalPostController');
Route::resource('v1.0/review', 'Api\v1_0\ReviewController');
*/
Route::apiResources([
    'v1.0/location' => 'Api\v1_0\LocationController',
    'v1.0/localpost' => 'Api\v1_0\LocalPostController',
    'v1.0/review' => 'Api\v1_0\ReviewController'
]);